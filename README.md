# Views: Mark Current Entity

Provides a Views argument handler which accepts an entity ID: it assumes this entity ID is the same entity type as the base_table of the current view. If it notices the ID passed in the argument matches the ID of a row in the results, it adds an HTML `data-views-mark-current="current-entity--row"` attribute to the row; and an HTML `data-views-mark-current="current-entity--field"` attribute to each field in the row.


## Requirements

This module requires no modules outside of Drupal core.


## Recommended modules

- [EVA: Entity Views Attachment](https://www.drupal.org/project/eva)


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Edit the view that you want to mark the current entity on.
2. Add the Contextual Filter (argument) with the name "Views mark current entity ID".

    If the view is embedded in an entity with the [EVA: Entity Views Attachment](https://www.drupal.org/project/eva) module, then there is usually nothing more to configure: this module can detect the entity it is attached to from the API provided by the EVA module.

    If the view is embedded some other way, then you will need to configure the Contextual Filter settings to get the current entity from some other context.

3. By default, this module will add a beige background color to the current entity. You may want to [override this in the theme layer](https://www.drupal.org/node/2216195#override-extend).

    See `css/views_mark_current_table.css` for an example of how to theme the current entity. 

4. By default, this module will add the text "(current)" to the title of the current entity. If you want to override this, see the documentation at the top of `js/views_mark_current_table.js`. 
