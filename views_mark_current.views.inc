<?php

/**
 * @file
 * Views hooks for the views_mark_current module.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_mark_current_views_data_alter(&$data) {
  // Define define our own, virtual field, and give it the argument handler
  // defined in
  // \Drupal\views_mark_current\Plugin\views\argument\ViewsMarkCurrentArgument.
  $virtualFieldName = 'views_mark_current_id';
  $virtualFieldDefinition = [
    'title' => t('Views mark current entity ID'),
    'help' => t('The ID of the entity to mark as the current one.'),
    'argument' => [
      'id' => 'views_mark_current_argument',
    ],
  ];

  // Add the virtual field to core entities which use ID columns that are not
  // named 'id'.
  if (isset($data['comment_field_data']['cid'])) {
    $data['comment_field_data'][$virtualFieldName] = $virtualFieldDefinition;
  }
  if (isset($data['node_field_data']['nid'])) {
    $data['node_field_data'][$virtualFieldName] = $virtualFieldDefinition;
  }
  if (isset($data['taxonomy_term_field_data']['tid'])) {
    $data['taxonomy_term_field_data'][$virtualFieldName] = $virtualFieldDefinition;
  }
  if (isset($data['users_field_data']['uid'])) {
    $data['users_field_data'][$virtualFieldName] = $virtualFieldDefinition;
  }

  // Loop through the views table data. We only want to process tables whose
  // name ends with '_field_data'; i.e.: fieldable entities.
  foreach ($data as $tableName => $tableData) {
    if (_views_mark_current_string_ends_with($tableName, '_field_data')) {
      // If the current table contains an 'id' field, add our virtual field.
      // Note the core entities which use ID columns that are not named 'id',
      // which we addressed earlier, will not match here.
      if (isset($data[$tableName]['id'])) {
        $data[$tableName][$virtualFieldName] = $virtualFieldDefinition;
      }
    }
  }

  return $data;
}
