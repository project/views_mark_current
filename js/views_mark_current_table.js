/**
 * @file
 * Client scripts for the views_mark_current module Table style.
 *
 * You can override this file in a custom module or theme by adding a snippet
 * similar to the following to your module or theme's .info.yml file:
 * @code
 * libraries-override:
 *   views_mark_current/table:
 *     js:
 *       js/views_mark_current_table.js: my/replacement_file.js
 * @endcode
 *
 * See https://www.drupal.org/node/2216195#override-extend for more information.
 */

(function ($, Drupal, once) {
  Drupal.behaviors.views_mark_current_table = {
    attach: function attach(context) {
      const $currentFields = $(
        once(
          'add-current-text',
          'td[data-views-mark-current="current-entity--field"]',
          context,
        ),
      );

      // This looks for a title field, and adds the translated text '(current)'
      // to the end of the field text.
      $currentFields
        .filter('.views-field-title')
        .each(function (index, element) {
          const $currentText = $('<span>')
            .addClass('views-mark-current-text')
            .attr('data-views-mark-current', 'current-text');
          $currentText.textContent = Drupal.t('(current)');
          $(element).append($currentText);
        });
    },
  };
})(jQuery, Drupal, once);
