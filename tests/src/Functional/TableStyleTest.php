<?php

namespace Drupal\Tests\views_mark_current\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test that views_mark_current can be used with the table style plugin.
 *
 * @group views_mark_current
 */
class TableStyleTest extends BrowserTestBase {

  /**
   * A selector for the views display we are testing.
   *
   * Note this contains the machine name of the view in the test module, and the
   * name of the views style plugin we are testing in this test.
   *
   * @var string
   */
  const VIEWS_DISPLAY_CSS_SELECTOR = '.views-element-container';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['views_mark_current_test'];

  /**
   * Tne first node, of type 'page', to use in the system under test.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $sutPageOne;

  /**
   * Tne second node, of type 'page', to use in the system under test.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $sutPageTwo;

  /**
   * Tne third node, of type 'article', to use in the system under test.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $sutArticleThree;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create two basic pages and an article.
    $this->sutPageOne = $this->drupalCreateNode(['type' => 'page']);
    $this->sutPageTwo = $this->drupalCreateNode(['type' => 'page']);
    $this->sutArticleThree = $this->drupalCreateNode(['type' => 'article']);
  }

  /**
   * Test what happens when you don't pass any current entity.
   *
   * Note testOneCurrentEntityNotInResults() tests different conditions, but a
   * similar outcome.
   */
  public function testNoCurrentEntity(): void {
    $assert = $this->assertSession();

    // Request the view without any arguments.
    $this->drupalGet('views_mark_current_test__table');
    $assert->statusCodeEquals(200);

    $resultViewTables = $this->getSession()->getPage()->findAll('css', self::VIEWS_DISPLAY_CSS_SELECTOR);
    $this->assertCount(1, $resultViewTables, 'The view is present and using the table style.');
    $resultViewTable = reset($resultViewTables);

    // Test that there are two rows displayed, containing data for sutPageOne
    // and sutPageTwo.
    $this->assertSession()->elementsCount('css', 'tr', 2, $resultViewTable);
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageOne->id());
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageOne->label());
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageTwo->id());
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageTwo->label());

    // Test that none of the rows are marked as the current entity.
    $this->assertSession()->elementsCount('css', 'tr[data-views-mark-current="current-entity--row"]', 0, $resultViewTable);
    $this->assertSession()->elementsCount('css', 'td[data-views-mark-current="current-entity--field"]', 0, $resultViewTable);
  }

  /**
   * Test passing one current entity that appears in the views results.
   */
  public function testOneCurrentEntityInResults(): void {
    $assert = $this->assertSession();

    // Request the view, passing the ID of the first basic page.
    $this->drupalGet(sprintf('views_mark_current_test__table/%s', $this->sutPageOne->id()));
    $assert->statusCodeEquals(200);

    $resultViewTables = $this->getSession()->getPage()->findAll('css', self::VIEWS_DISPLAY_CSS_SELECTOR);
    $this->assertCount(1, $resultViewTables, 'The view is present and using the table style.');
    $resultViewTable = reset($resultViewTables);

    // Test that there are two rows displayed, containing data for sutPageOne
    // and sutPageTwo.
    $this->assertSession()->elementsCount('css', 'tr', 2, $resultViewTable);
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageOne->id());
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageOne->label());
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageTwo->id());
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageTwo->label());

    // Test that only one row (with 2 columns) are marked as the current entity.
    $this->assertSession()->elementsCount('css', 'tr[data-views-mark-current="current-entity--row"]', 1, $resultViewTable);
    $this->assertSession()->elementsCount('css', 'td[data-views-mark-current="current-entity--field"]', 2, $resultViewTable);

    // Test that the marked fields contain sutPageOne's data...
    $this->assertSession()->elementTextContains('css', 'td.views-field-nid[data-views-mark-current="current-entity--field"]', $this->sutPageOne->id());
    $this->assertSession()->elementTextContains('css', 'td.views-field-title[data-views-mark-current="current-entity--field"]', $this->sutPageOne->label());

    // ... and not sutPageTwo's data.
    $this->assertSession()->elementTextNotContains('css', 'td.views-field-nid[data-views-mark-current="current-entity--field"]', $this->sutPageTwo->id());
    $this->assertSession()->elementTextNotContains('css', 'td.views-field-title[data-views-mark-current="current-entity--field"]', $this->sutPageTwo->label());
  }

  /**
   * Test passing one current entity that does not appear in the views results.
   *
   * Note testNoCurrentEntity() tests different conditions; but a similar
   * outcome.
   */
  public function testOneCurrentEntityNotInResults(): void {
    $assert = $this->assertSession();

    // Request the view, passing the ID of the article.
    $this->drupalGet(sprintf('views_mark_current_test__table/%s', $this->sutArticleThree->id()));
    $assert->statusCodeEquals(200);

    $resultViewTables = $this->getSession()->getPage()->findAll('css', self::VIEWS_DISPLAY_CSS_SELECTOR);
    $this->assertCount(1, $resultViewTables, 'The view is present and using the table style.');
    $resultViewTable = reset($resultViewTables);

    // Test that there are two rows displayed, containing data for sutPageOne
    // and sutPageTwo.
    $this->assertSession()->elementsCount('css', 'tr', 2, $resultViewTable);
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageOne->id());
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageOne->label());
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageTwo->id());
    $this->assertSession()->elementTextContains('css', self::VIEWS_DISPLAY_CSS_SELECTOR, $this->sutPageTwo->label());

    // Test that none of the rows are marked as the current entity.
    $this->assertSession()->elementsCount('css', 'tr[data-views-mark-current="current-entity--row"]', 0, $resultViewTable);
    $this->assertSession()->elementsCount('css', 'td[data-views-mark-current="current-entity--field"]', 0, $resultViewTable);
  }

}
