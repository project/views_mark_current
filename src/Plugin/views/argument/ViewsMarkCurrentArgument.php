<?php

namespace Drupal\views_mark_current\Plugin\views\argument;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;

/**
 * Accept the current entity ID(s) for highlighting/marking.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("views_mark_current_argument")
 */
class ViewsMarkCurrentArgument extends ArgumentPluginBase {

  /**
   * A list of entities to consider the "current" ones.
   *
   * @var array
   */
  public $currentEntities = [];

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Exceptions when an argument is not provided do not make sense here, so
    // disable the fieldset and programmatically set the form values.
    $form['exception']['#access'] = FALSE;
    $form['exception']['value']['#value'] = '';
    $form['exception']['title_enable']['#value'] = FALSE;
    $form['exception']['title']['#value'] = '';
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultActions($which = NULL) {
    $defaults = parent::defaultActions($which);

    // Reword some base default actions to describe what they do in this
    // context.
    $defaults['ignore']['title'] = $this->t('Do not mark any rows as the current row');

    // Unset some base default actions which do not make sense, or would cause
    // unexpected behavior in this context.
    unset($defaults['summary']);

    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function postExecute(&$values) {
    // If the data passed to this argument is an integer or string, then assume
    // that it is an entity ID, and save it to the array of current entities.
    if (is_int($this->argument) || is_string($this->argument)) {
      $this->currentEntities[$this->argument] = $this->argument;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    // No-op: parent::query() adds a WHERE condition which filters out the
    // entities passed as argument(s). But we *do* want the entities passed as
    // arguments in the results, i.e.: we don't want to affect the query.
  }

}
